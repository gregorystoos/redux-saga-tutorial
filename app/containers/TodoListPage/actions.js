/*
 *
 * TodoListPage actions
 *
 */

import { ADD_TODO,DELETE_TODO, LOAD_LIST } from './constants';

export function addTodoAction(title) {
  return {
    type: ADD_TODO,
    title
  };
}

export function deleteTodoAction(id) {
  return {
    type: DELETE_TODO,
    id
  };
}

export function loadListAction() {
  return {
    type: LOAD_LIST
  };
}
