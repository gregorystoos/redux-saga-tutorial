/*
 *
 * TodoListPage constants
 *
 */

export const ADD_TODO = 'app/TodoListPage/ADD_TODO';
export const LOAD_LIST = 'app/TodoListPage/LOAD_LIST';
export const FETCH_SUCCEEDED = 'app/TodoListPage/FETCH_SUCCEEDED';
export const FETCH_FAILED = 'app/TodoListPage/FETCH_FAILED';
export const ADD_FAILED = 'app/TodoListPage/ADD_FAILED';
export const DELETE_TODO = 'app/TodoListPage/DELETE_TODO';
