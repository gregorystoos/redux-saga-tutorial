/**
 *
 * Asynchronously loads the component for TodoListPage
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
