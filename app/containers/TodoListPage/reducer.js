/*
 *
 * TodoListPage reducer
 *
 */

import { fromJS } from 'immutable';
import { ADD_TODO, FETCH_SUCCEEDED } from './constants';

export const initialState = fromJS({todos: []});

function todoListPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_SUCCEEDED:
   		var list = action.data;
   		if(!list){ list = []; }
   		return state.set('todos', fromJS(list));
    default:
      return state;
  }
}

export default todoListPageReducer;
