import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the todoListPage state domain
 */

const selectTodoListPageDomain = state =>
  state.get('todoListPage', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by TodoListPage
 */

const makeSelectTodoListPage = () =>
  createSelector(selectTodoListPageDomain, substate => substate.get('todos').toJS());

export default makeSelectTodoListPage;
export { selectTodoListPageDomain };
