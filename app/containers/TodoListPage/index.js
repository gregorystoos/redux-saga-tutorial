/**
 *
 * TodoListPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectTodoListPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import { addTodoAction, loadListAction, deleteTodoAction } from './actions';

import _ from 'underscore';

/* eslint-disable react/prefer-stateless-function */
export class TodoListPage extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      inputVal: ''
    };

    this.changeInputVal = this.changeInputVal.bind(this);
    this.addTodo = this.addTodo.bind(this);

  }

  componentDidMount(){
    this.props.loadListAction();
  }

  changeInputVal(e){
    this.setState({inputVal: e.target.value});
  }

  addTodo(){
    this.props.addTodoAction(this.state.inputVal);
    this.setState({inputVal: ''});
  }

  render() {

    var list = _.map(this.props.todoList, task=>{
      return <li key={task.id}>{task.task} <a onClick={()=>{ this.props.deleteTodoAction(task.id); }}>delete</a></li>
    });


    return (
      <div>
        <Helmet>
          <title>TodoListPage</title>
          <meta name="description" content="Description of TodoListPage" />
        </Helmet>
        <FormattedMessage {...messages.header} />
        <ul>{list}</ul>

        <input type="text" value={this.state.inputVal} onChange={this.changeInputVal} />
        <button onClick={this.addTodo}>ADD</button>
      </div>
    );
  }
}

TodoListPage.propTypes = {
  addTodoAction: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  todoList: makeSelectTodoListPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    addTodoAction: title => dispatch(addTodoAction(title)),
    deleteTodoAction: id => dispatch(deleteTodoAction(id)),
    loadListAction: () => dispatch(loadListAction()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'todoListPage', reducer });
const withSaga = injectSaga({ key: 'todoListPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(TodoListPage);
