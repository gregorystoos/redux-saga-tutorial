import { call, put, takeLatest, takeEvery } from 'redux-saga/effects';
import { ADD_TODO, DELETE_TODO, LOAD_LIST, FETCH_SUCCEEDED, FETCH_FAILED, ADD_FAILED } from './constants';

import request from 'utils/request';


export function* getList(){
	try {
		const data = yield call(request, 'http://todo.dev01.squaredbyte.com/api/all-data');
		yield put({type: FETCH_SUCCEEDED, data: data.data});
	}
	catch (error) {
		yield put({type: FETCH_FAILED, error});
	}
}

export function* addTodo(action){
	try {
		const data = yield call(
			request, 
			'http://todo.dev01.squaredbyte.com/api/insert',
			{
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({task: action.title})
			}
		);
		yield put({type: LOAD_LIST});
	}
	catch (error) {
		yield put({type: ADD_FAILED, error});
	}
}


export function* deleteTodo(action){
	try {
		const data = yield call(
			request, 
			'http://todo.dev01.squaredbyte.com/api/delete/' + action.id
		);
		yield put({type: LOAD_LIST});
	}
	catch (error) {
		yield put({type: ADD_FAILED, error});
	}
}


export default function* todoListPageSaga() {
  yield takeLatest(LOAD_LIST, getList);
  yield takeEvery(ADD_TODO, addTodo);
  yield takeEvery(DELETE_TODO, deleteTodo);
}
