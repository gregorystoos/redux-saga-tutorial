import { fromJS } from 'immutable';
import todoListPageReducer from '../reducer';

describe('todoListPageReducer', () => {
  it('returns the initial state', () => {
    expect(todoListPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
